import { AssignmentsModule } from './assignments/assignments.module';
import { CameraModule } from './camera/camera.module';
import { VehicleModule } from './vehicle/vehicle.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers';
import { ReactiveFormsModule } from '@angular/forms';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    StoreModule.forRoot(reducers),
    VehicleModule,
    CameraModule,
    AssignmentsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
