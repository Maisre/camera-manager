import { Component, OnInit } from '@angular/core';
import * as actions from '../vehicle.actions';
import * as vehicleReducer from '../vehicle.reducer';
import { Observable } from 'rxjs';
import { Vehicle } from '../vehicle.reducer';
import { Store } from '@ngrx/store';
import { FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {
  vehs: Vehicle[];

  newVehicleForm = this.fb.group({
    vehName: ['', [Validators.required, this.noDuplicate.bind(this)]],
  });

  public vehicles: Observable<Vehicle[]>;
  constructor(private vehStore: Store<vehicleReducer.State>, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.vehicles = this.vehStore.select(vehicleReducer.selectAll);
    this.vehicles.subscribe((vehs) => this.vehs = vehs);
  }

  public createNew() {
    this.vehStore.dispatch(new actions.Create(this.newVehicleForm.get('vehName').value));
    this.newVehicleForm.reset();
  }

  private noDuplicate(control: FormControl) {
    const newVehicleName = control.value;
    if (newVehicleName && this.vehs && this.vehs.find((v) => v.name === newVehicleName) ) {
      return {duplicateName: true };
    }
    return null;
  }
}
