import { Action } from '@ngrx/store';
import { Vehicle } from './vehicle.reducer';

export const CREATE = '[Vehicle] Create';
export const UPDATE = '[Vehicle] Update';
export const DELETE = '[Vehicle] Delete';

export class Create implements Action {
  readonly type = CREATE;
  constructor(public name: string) { }
}

export class Update implements Action {
  readonly type = UPDATE;
  constructor(
    public id: number,
    public changes: Partial<Vehicle>
  ) { }
}

export class Delete implements Action {
  readonly type = DELETE;
  constructor(public id: number) {}
}

export type VehicleActions = Create | Update | Delete;
