import { EntityState, createEntityAdapter } from '@ngrx/entity';
import * as actions from './vehicle.actions';
import { createFeatureSelector } from '@ngrx/store';

export class Vehicle {
  static nextId = 3;
  id: number;
  name: string;
}

export const vehicleAdapter = createEntityAdapter<Vehicle>();
export interface State extends EntityState<Vehicle> {}

const defaultVehicles = {
  ids: [1, 2],
  entities: {
    1: {
      id: 1,
      name: 'Cool Truck'
    },
    2: {
      id: 2,
      name: 'Not So Cool',
    }
  }
};

export const initialState: State = vehicleAdapter.getInitialState(defaultVehicles);

export function vehicleReducer(
  state: State = initialState,
  action: actions.VehicleActions,
) {
  switch ( action.type ) {
    case actions.CREATE:
      const newVeh = {id: Vehicle.nextId, name: action.name };
      Vehicle.nextId++;
      return vehicleAdapter.addOne(newVeh, state);
    case actions.UPDATE:
      return vehicleAdapter.updateOne({
        id: action.id,
        changes: action.changes,
      }, state);
    case actions.DELETE:
      return vehicleAdapter.removeOne(action.id, state);
    default:
      return state;
  }
}

export const getVehicleState = createFeatureSelector<State>('vehicle');

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = vehicleAdapter.getSelectors(getVehicleState);
