import { CameraComponent } from './camera/camera/camera.component';
import { VehicleComponent } from './vehicle/vehicle/vehicle.component';
import { AssignmentComponent } from './assignments/assignment/assignment.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  {path: '', component: AssignmentComponent},
  {path: 'vehicles', component: VehicleComponent},
  {path: 'cameras', component: CameraComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
