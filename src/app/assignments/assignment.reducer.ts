
import * as actions from './assignment.actions';
import { EntityState, createEntityAdapter } from '@ngrx/entity';
import { createFeatureSelector } from '@ngrx/store';
import { Vehicle } from '../vehicle/vehicle.reducer';
import { Camera } from '../camera/camera.reducer';

export class Assignment {
  static nextId = 1;
  id: number;
  cameraId: number;
  vehicleId: number;
  dateCreated: Date;
  deleted: boolean;
}

export class HydratedAssignment {
  id: number;
  cameraId: number;
  deviceNo: string;
  vehicleId: number;
  name: string;
  dateCreated: Date;
  deleted: boolean;

  constructor(assignment: Assignment, vehicle: Vehicle, camera: Camera) {
    Object.assign(this, assignment);
    this.deviceNo = camera.deviceNo;
    this.name = vehicle.name;
  }
}

export const assignmentAdapter = createEntityAdapter<Assignment>();
export interface State extends EntityState<Assignment> {}

const defaultAssignments = {};

export const initialState: State = assignmentAdapter.getInitialState(defaultAssignments);

export function assignmentReducer(
  state: State = initialState,
  action: actions.AssignmentActions,
) {
  switch ( action.type ) {
    case actions.CREATE:
      const newAssignment = {
        id: Assignment.nextId,
        cameraId: action.cameraId,
        vehicleId: action.vehicleId,
        dateCreated: new Date(),
        deleted: false };

      Assignment.nextId++;
      return assignmentAdapter.addOne(newAssignment, state);
    case actions.UPDATE:
      return assignmentAdapter.updateOne({
        id: action.id,
        changes: action.changes,
      }, state);
    case actions.DELETE:
      const deletedChanges = {
        id: action.id,
        changes: { deleted: true },
      };
      return assignmentAdapter.updateOne(deletedChanges, state);
    default:
      return state;
  }
}
export const getAssignmentState = createFeatureSelector<State>('assignment');

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = assignmentAdapter.getSelectors(getAssignmentState);
