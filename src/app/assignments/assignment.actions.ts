import { Action } from '@ngrx/store';
import { Assignment } from './assignment.reducer';

export const CREATE = '[Assignments] Create';
export const UPDATE = '[Assignments] Update';
export const DELETE = '[Assignments] Delete';

export class Create implements Action {
  readonly type = CREATE;
  constructor(public vehicleId: number, public cameraId: number) { }
}

export class Update implements Action {
  readonly type = UPDATE;
  constructor(
    public id: number,
    public changes: Partial<Assignment>
  ) { }
}

export class Delete implements Action {
  readonly type = DELETE;
  constructor(public id: number) {}
}

export type AssignmentActions = Create | Update | Delete;
