import { HydratedAssignment } from './../assignment.reducer';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import * as actions from '../assignment.actions';
import * as assignmentReducer from '../assignment.reducer';
import * as cameraReducer from '../../camera/camera.reducer';
import * as vehicleReducer from '../../vehicle/vehicle.reducer';
import { Observable, combineLatest } from 'rxjs';
import { Vehicle } from '../../vehicle/vehicle.reducer';
import { Camera } from '../../camera/camera.reducer';
import { Assignment } from '../assignment.reducer';
import { map, startWith, debounceTime, tap } from 'rxjs/operators';

@Component({
  selector: 'app-assignment',
  templateUrl: './assignment.component.html',
  styleUrls: ['./assignment.component.css']
})
export class AssignmentComponent implements OnInit {

  public assignments: Observable<Assignment[]>;
  public vehicles: Observable<Vehicle[]>;
  public cameras: Observable<Camera[]>;
  public availableVehicles: any[];
  public availableCameras: any[];
  public searchField = new FormControl();
  public hydratedAssignments: HydratedAssignment[];
  public filteredHydratedAssignments: Observable<HydratedAssignment[]>;

  filteredCameraOptions: Observable<Camera[]>;
  filteredVehicleOptions: Observable<Vehicle[]>;
  newAssignmentForm = this.fb.group({
    vehicle: ['', Validators.required],
    camera: ['', Validators.required],
  });

  constructor(
    private assignmentStore: Store<assignmentReducer.State>,
    private cameraStore: Store<cameraReducer.State>,
    private vehStore: Store<vehicleReducer.State>,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.assignments = this.assignmentStore.select(assignmentReducer.selectAll);
    this.vehicles = this.vehStore.select(vehicleReducer.selectAll);
    this.cameras = this.cameraStore.select(cameraReducer.selectAll);
    this.filteredCameraOptions = this.newAssignmentForm.get('camera').valueChanges.pipe(
      startWith(''),
      map(value => this.filterCameras(value))
    );
    this.filteredVehicleOptions = this.newAssignmentForm.get('vehicle').valueChanges.pipe(
      startWith(''),
      map(value => this.filterVehicles(value))
    );

    this.filteredHydratedAssignments = this.searchField.valueChanges.pipe(
      debounceTime(200),
      map(value => {
        if (value === '') {
          return this.hydratedAssignments.filter(h => true);
        }
        return this.hydratedAssignments.filter(h => h.name.toLowerCase().includes(value.toLowerCase())).concat(
        this.hydratedAssignments.filter(h => h.deviceNo.toLowerCase().includes(value.toLowerCase()))).filter((v, i, s) =>
          s.indexOf(v) === i);
      }
        )
    );

    combineLatest([this.assignments, this.cameras, this.vehicles]).subscribe(
      ([assignments, cameras, vehicles]) => {
        this.availableCameras = cameras.filter((cam) => !assignments.filter(a => !a.deleted).find((a) => a.cameraId === cam.id));
        this.availableVehicles = vehicles.filter((veh) => !assignments.filter(a => !a.deleted).find((a) => a.vehicleId === veh.id));
        this.hydratedAssignments = assignments.map((a) => {
          return new HydratedAssignment(a, vehicles.find((v) => v.id === a.vehicleId), cameras.find((c) => c.id === a.cameraId));
        });
        this.newAssignmentForm.get('camera').setValue('');
        this.newAssignmentForm.get('vehicle').setValue('');
      }
    );
    setTimeout(() => this.searchField.setValue(''), 100);
  }


  public createNew() {
    this.assignmentStore.dispatch(
      new actions.Create(this.newAssignmentForm.get('vehicle').value.id, this.newAssignmentForm.get('camera').value.id));
    this.newAssignmentForm.reset();
    this.searchField.setValue('');
  }

  public remove(assignmentID: number) {
    this.assignmentStore.dispatch(
      new actions.Delete(assignmentID));
    this.searchField.setValue('');
  }

  cameraDisplayFn(cam: Camera): string {
    return cam && cam.deviceNo ? cam.deviceNo : '';
  }

  vehicleDisplayFn(veh: Vehicle): string {
    return veh && veh.name ? veh.name : '';
  }

  private filterCameras(value: any): Camera[] {
    if (!value || value.id) {
      value = '';
    }
    const filterValue = value.toLowerCase();

    return this.availableCameras.filter(option => option.deviceNo.toLowerCase().indexOf(filterValue) === 0);
  }
  private filterVehicles(value: any): Vehicle[] {
    if (!value || value.id) {
      value = '';
    }
    const filterValue = value.toLowerCase();

    return this.availableVehicles.filter(option => option.name.toLowerCase().indexOf(filterValue) === 0);
  }

}
