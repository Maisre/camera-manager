import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { FormBuilder, Validators, FormControl } from '@angular/forms';
import { Camera } from '../camera.reducer';
import { Store } from '@ngrx/store';
import * as actions from '../camera.actions';
import * as cameraReducer from '../camera.reducer';

@Component({
  selector: 'app-camera',
  templateUrl: './camera.component.html',
  styleUrls: ['./camera.component.css']
})
export class CameraComponent implements OnInit {
  cams: Camera[];

  newCameraForm = this.fb.group({
    deviceNo: ['', [Validators.required, this.noDuplicate.bind(this)]],
  });

  public cameras: Observable<Camera[]>;
  constructor(private cameraStore: Store<cameraReducer.State>, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.cameras = this.cameraStore.select(cameraReducer.selectAll);
    this.cameras.subscribe((cams) => this.cams = cams);
  }

  public createNew() {
    this.cameraStore.dispatch(new actions.Create(this.newCameraForm.get('deviceNo').value));
    this.newCameraForm.reset();
  }

  private noDuplicate(control: FormControl) {
    const newDeviceNo = control.value;
    if (newDeviceNo && this.cams && this.cams.find((c) => c.deviceNo === newDeviceNo) ) {
      return {duplicateName: true };
    }
    return null;
  }
}
