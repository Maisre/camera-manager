import { Action } from '@ngrx/store';
import { Camera } from './camera.reducer';

export const CREATE = '[Camera] Create';
export const UPDATE = '[Camera] Update';
export const DELETE = '[Camera] Delete';

export class Create implements Action {
  readonly type = CREATE;
  constructor(public deviceNo: string) { }
}

export class Update implements Action {
  readonly type = UPDATE;
  constructor(
    public id: number,
    public changes: Partial<Camera>
  ) { }
}

export class Delete implements Action {
  readonly type = DELETE;
  constructor(public id: number) {}
}

export type CameraActions = Create | Update | Delete;
