import { EntityState, createEntityAdapter } from '@ngrx/entity';
import * as actions from './camera.actions';
import { createFeatureSelector } from '@ngrx/store';

export class Camera {
  static nextId = 3;
  id: number;
  deviceNo: string;
}

export const cameraAdapter = createEntityAdapter<Camera>();
export interface State extends EntityState<Camera> {}

const defaultCameras = {
  ids: [1, 2],
  entities: {
    1: {
      id: 1,
      deviceNo: 'CAM_1'
    },
    2: {
      id: 2,
      deviceNo: 'CAM_2',
    }
  }
};

export const initialState: State = cameraAdapter.getInitialState(defaultCameras);

export function cameraReducer(
  state: State = initialState,
  action: actions.CameraActions,
) {

  switch ( action.type ) {
    case actions.CREATE:
      const newCamera = {id: Camera.nextId, deviceNo: action.deviceNo };
      Camera.nextId++;
      return cameraAdapter.addOne(newCamera, state);
    case actions.UPDATE:
      return cameraAdapter.updateOne({
        id: action.id,
        changes: action.changes,
      }, state);
    case actions.DELETE:
      return cameraAdapter.removeOne(action.id, state);
    default:
      return state;
  }
}

export const getCameraState = createFeatureSelector<State>('camera');

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = cameraAdapter.getSelectors(getCameraState);
